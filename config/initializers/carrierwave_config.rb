CarrierWave.configure do |config|
  config.fog_credentials = {
    :provider               => 'AWS',                          # required
    :aws_access_key_id      => ENV["AWS_ACCESS_KEY_ID"],       # required
    :aws_secret_access_key  => ENV["AWS_SECRET_ACCESS_KEY"],   # required
    :host                   => 's3.amazonaws.com'              # optional, defaults to nil
    # :endpoint               => 'https://s3.example.com:8080' # optional, defaults to nil
  }
  config.store_dir = nil
  config.fog_directory  = 'lekevicius-christmas-drawings'         # required
  config.fog_attributes = {'Cache-Control'=>'max-age=315576000'}  # optional, defaults to {}
end
