Christmas2013::Application.routes.draw do
  root 'drawings#new', as: 'new_drawing'
  get '/postcards' => 'drawings#index', as: 'drawings'
  post '/postcards' => 'drawings#create'
  get '/postcard/:url' => 'drawings#show', as: 'drawing'
end
