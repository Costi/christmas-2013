class DrawingUploader < CarrierWave::Uploader::Base

  include CarrierWave::MiniMagick
  
  storage :fog
  def fog_directory
    Rails.env.production? ? 'lekevicius-christmas-drawings' : 'lekevicius-christmas-drawings-dev'
  end

  version :thumb do
    process :resize_to_fill => [240, 180]
  end

  def filename
    random_token = SecureRandom.urlsafe_base64
    ivar = "@#{mounted_as}_secure_token"
    token = model.instance_variable_get(ivar)
    token ||= model.instance_variable_set(ivar, random_token)
    "#{token}.jpg" if original_filename
  end

  def extension_white_list
    %w(jpg jpeg)
  end

end